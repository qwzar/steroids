extern crate csv;

use std::thread;
use std::ffi::{CStr};
use std::os::raw::c_char;
use std::fs::File;
use csv::{ReaderBuilder};



const KZT: f32 = 346.40;
const RUB: f32 = 63.48;
const MNT: f32 = 2456.28;

#[no_mangle]
pub extern fn parse(path_name: *const c_char) {
    let path = unsafe { CStr::from_ptr(path_name).to_str().unwrap()};
    let file = File::open(path).expect("Couldn't open csv file");
    let mut csv_file = ReaderBuilder::new().has_headers(false).from_reader(file);
    
    for result in csv_file.records() {
        let record = result.unwrap();
        let replacer = str::replace(&record[1], ",", "");
        let number: Option<f32> = replacer.parse().ok();
        match number {
            Some(price) => {bitcoin_count(price)},
            _ => println!("WTF, it's not a number"),
        }
    }
}

fn bitcoin_count(price: f32) {
    let handles: Vec<_> = vec![100, 200, 300].into_iter().map(|p| {
        thread::spawn(move || {
            let mut result: [f32; 3] = [0.0; 3]; //Some thing
            println!("{}", p);
            for _ in 1..3 {
                result[0] = &KZT * price * p as f32;
                result[1] = &RUB * price * p as f32;
                result[2] = &MNT * price * p as f32;
            }
            result
        })
    }).collect();

    for h in handles {
        println!("bitoc[kzt,usd,mnt]={:?}",
        h.join().map_err(|_| "NO RUST! PLEASE NO!!! NOOOOOOOOOO").unwrap());
    }
}